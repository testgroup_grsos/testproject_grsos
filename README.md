# TestProject_GRSOS

This is a test repo for an open source software.


## Installation
Please dollow the installation guide on how to install the software.

## Usage
This software is free for usage according to the Licece agreement.

## Contributing
Feel free to contribute, write Issues or create Merge Requests.

## Authors and acknowledgment
Authors:
- Me
- Collegues

Acknowledgment to the funding agencies. Thank you.

## License
THis is licenced under an Open Source Licence.
